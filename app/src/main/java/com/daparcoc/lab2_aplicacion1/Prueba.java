package com.daparcoc.lab2_aplicacion1;

public class Prueba {
    String string = "Programación";

    // Con este modificador cualquier clase puede acceder a esta variable
    // y ya no es necesario los métodos set and get.
    public String dato;

    String nueva_cadena = Modelo.cadena;

    public Prueba() {

    }

    public void ejecutar() {
        String var = dato;
    }

    public void iterador (String[] string) {
        // No es preferible usar esto
        //for (int i = 1; i <= 10; i++) {

        //}

        for (String s : string) {

        }
    }

    // Se borra la clase privada
    /*private class ClasePrivada {
        public void accion() {
            ejecutar();
        }
    }*/

    // Debido a que no es buena idea
    /*public void setDato(String s) {
        dato = s;
    }

    public String getDato() {
        return dato;
    }*/
}
